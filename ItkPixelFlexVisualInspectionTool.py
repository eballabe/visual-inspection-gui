#!/usr/bin/env python

import os
import sys
import time
import subprocess
from datetime import datetime
#import upload_results
from PIL.ImageQt import ImageQt


if False:
    import signal
    req_enum="%(LCG_INST_PATH)s/LCG_94/enum34/1.1.6/%(CMTCONFIG)s/lib/python2.7/site-packages"%os.environ
    sys.path.insert(0,req_enum)
    pass

from PyQt5 import QtWidgets
from PyQt5 import QtCore
from PyQt5 import QtGui

from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QGridLayout, QWidget

#CPx=0
#CPy=0

class PixFlexVisInsp(QtWidgets.QMainWindow):

    def __init__(self):
        super(PixFlexVisInsp, self).__init__()
        #QtWidgets.QApplication.setStyle("Plastique")
        self.setWindowTitle("ITK Pixel - Flex Visual Inspection Tool")
        #self.setGeometry(0,0,900,660)
        self.setFixedSize(940, 660)
        #self.setMouseTracking(True)
        self.setMouseMoveEventDelegate(self)
        self.create()
        
    def create(self):

        widMain = QtWidgets.QWidget(self)
        self.widMain=widMain
        self.setCentralWidget(widMain)        

        menubar = self.menuBar()
        self.fileMenu = menubar.addMenu("Help")
        self.fileMenu = menubar.addMenu("About")
        #self.fileMenu.addAction(exitAct)

        self.bLoadFields = QtWidgets.QPushButton("Load Fields",widMain)
        self.bLoadFields.clicked.connect(self.loadFields) 
        self.bSaveFields = QtWidgets.QPushButton("Save Fields",widMain)
        self.bSaveFields.clicked.connect(self.saveFields) 

        self.bLoadImages = QtWidgets.QPushButton("Load Images",widMain)
        self.bLoadImages.resize(150,50)
        self.bLoadImages.clicked.connect(self.zoomImg) 
        self.bGenerateReport = QtWidgets.QPushButton("Generate Report",widMain)
        self.bGenerateReport.resize(150,50)
        self.bGenerateReport.clicked.connect(self.GenRep)
        self.bUploadToPDB = QtWidgets.QPushButton("Upload to Prod DB",widMain)
        self.bUploadToPDB.clicked.connect(self.UploadToPDB)
        self.bUploadToPDB.resize(165,50)
        
        self.flexVersion="0.0"
        self.lFlexVersion = QtWidgets.QLabel("Flex Version:",widMain)
        self.rbFlexV2p2 = QtWidgets.QRadioButton("2.2",widMain)
        self.rbFlexV3p8 = QtWidgets.QRadioButton("3.8",widMain)
        self.lID = QtWidgets.QLabel("Flex ID:",widMain)
        self.tID = QtWidgets.QLineEdit(widMain)
        self.lDeliveryDate = QtWidgets.QLabel("Delivery Date:",widMain)
        self.tDeliveryDate = QtWidgets.QDateEdit(widMain)
        now=QtCore.QDate.currentDate()
        self.tDeliveryDate.setDate(now)
        self.lOrigin = QtWidgets.QLabel("Origin:",widMain)
        self.cbOrigin = QtWidgets.QComboBox(widMain)
        self.cbOrigin.addItems(["UK Glasgow", "Site B", "Site C"])
        self.cbOrigin.resize(140,30)
        #self.cbOrigin.resize(130,25)
        self.lProducedBy = QtWidgets.QLabel("Produced By:",widMain)
        self.tProducedBy = QtWidgets.QLineEdit(widMain)
        self.lAssemblySite = QtWidgets.QLabel("Assembly Site:",widMain)
        self.cbAssemblySite = QtWidgets.QComboBox(widMain)
        self.cbAssemblySite.addItems(["CERN", "Site B", "Site C"])
        self.cbAssemblySite.resize(140,30)
        self.lCleanedBy = QtWidgets.QLabel("Cleaned By:",widMain)
        self.tCleanedBy = QtWidgets.QLineEdit(widMain)
        self.lStorageSite = QtWidgets.QLabel("Storage Site:",widMain)
        self.cbStorageSite = QtWidgets.QComboBox(widMain)
        self.cbStorageSite.addItems(["CERN DSF", "CERN SMD Lab", "Site C"])
        self.cbStorageSite.resize(140,30)
        self.lInspectionDate = QtWidgets.QLabel("Inspection Date:",widMain)
        self.tInspectionDate = QtWidgets.QDateEdit(widMain)
        self.tInspectionDate.setDate(now)
        self.lInspectionSite = QtWidgets.QLabel("Inspection Site:",widMain)
        self.cbInspectionSite = QtWidgets.QComboBox(widMain)
        self.cbInspectionSite.addItems(["CERN DSF", "Site B", "Site C"])
        self.cbInspectionSite.resize(140,30)
        self.lInspector = QtWidgets.QLabel("Inspector:",widMain)
        self.tInspector = QtWidgets.QLineEdit(widMain)
        self.lGeneralComments = QtWidgets.QLabel("General Comments:",widMain)
        self.tGeneralComments = QtWidgets.QTextEdit(widMain)
        self.tGeneralComments.setLineWrapColumnOrWidth(285)
        self.tGeneralComments.setTextColor(QtGui.QColor(255,255,255,255))
        self.tGeneralComments.resize(285,90)
        self.lObservations = QtWidgets.QLabel("Observations:",widMain)
        self.tObservations = QtWidgets.QTextEdit(widMain)
        self.tObservations.resize(285,90)
        
        self.dir_TileImages = "/Users/ericballabene/Inspection_Images/RD53A_Quad_HPK_11_Assembly"

        x=170
        y=55
        self.bLoadFields.move(x-160,y-50)
        self.bSaveFields.move(x,y-50)

        self.bLoadImages.move(x+160,y-50)
        self.bGenerateReport.move(x+160+160+30+60,y-50)
        self.bUploadToPDB.move(900-10-165,y-50)

        self.lFlexVersion.move(x-160,y+30*0)
        self.rbFlexV2p2.move(x,y+30*0)
        self.rbFlexV3p8.move(x+60,y+30*0)
        self.lID.move(x-160,y+30*1)
        #self.lID.setAlignment(QtCore.Qt.AlignRight)
        self.tID.move(x,y+30*1)
        self.lDeliveryDate.move(x-160,y+30*2)
        self.tDeliveryDate.move(x,y+30*2)
        self.lOrigin.move(x-160,y+30*3)
        self.cbOrigin.move(x-7,y+30*3)
        self.lProducedBy.move(x-160,y+30*4)
        self.tProducedBy.move(x,y+30*4)
        self.lAssemblySite.move(x-160,y+30*5)
        self.cbAssemblySite.move(x-7,y+30*5)
        self.lCleanedBy.move(x-160,y+30*6)
        self.tCleanedBy.move(x,y+30*6)
        self.lStorageSite.move(x-160,y+30*7)
        self.cbStorageSite.move(x-7,y+30*7)
        self.lInspectionDate.move(x-160,y+30*8)
        self.tInspectionDate.move(x,y+30*8)
        self.lInspectionSite.move(x-160,y+30*9)
        self.cbInspectionSite.move(x-7,y+30*9)
        self.lInspector.move(x-160,y+30*10)
        self.tInspector.move(x,y+30*10)
        self.lGeneralComments.move(x-160,y+30*11)
        self.tGeneralComments.move(x-160,y+30*11+20)
        self.lObservations.move(x-160,y+30*12+90)
        self.tObservations.move(x-160,y+30*12+110)
        #self.bRead[psu].clicked.connect(lambda state, x=psu : self.read(x))
        #self.bWrite[psu].clicked.connect(lambda state, x=psu : self.write(x))
        self.statusBar().showMessage("Loaded")
        
        self.box = QtWidgets.QLabel(self)
        self.setMouseMoveEventDelegate(self.box)
        self.box.move(350,100)
        self.box.resize(300,87)
        self.addGridImage_3()
        self.create_tiles()
        self.tileMenu()
        self.show()

        self.rbFlexV3p8.setChecked(True) 
        if self.rbFlexV2p2.isChecked() == True: self.flexVersion="2.2"
        elif self.rbFlexV3p8.isChecked() == True: self.flexVersion="3.8"

        self.rbFlexV2p2.clicked.connect(self.setup_FlexV2p2) 
        self.rbFlexV3p8.clicked.connect(self.setup_FlexV3p8) 

        pass


    def setup_FlexV2p2(self):

        pixmap2 = QtGui.QPixmap("Mapping.png")
        self.grid.setPixmap(pixmap2)
        self.grid.resize(pixmap2.width(),pixmap2.height())

        if self.rbFlexV2p2.isChecked() == True: self.flexVersion="2.2"
        elif self.rbFlexV3p8.isChecked() == True: self.flexVersion="3.8"
        print("Switch to flex version = ",self.flexVersion)
        
        self.tileMenu()
        self.show()

        pass

    def setup_FlexV3p8(self):

        pixmap2 = QtGui.QPixmap("Mapping_38.png")
        self.grid.setPixmap(pixmap2)
        self.grid.resize(pixmap2.width(),pixmap2.height())

        if self.rbFlexV2p2.isChecked() == True: self.flexVersion="2.2"
        elif self.rbFlexV3p8.isChecked() == True: self.flexVersion="3.8"
        print("Switch to flex version = ",self.flexVersion)

        self.tileMenu()
        self.show()

        pass


    def addGridImage_3(self):
        global Imgx, Imgy
        self.grid = QtWidgets.QLabel(self)
        self.setMouseMoveEventDelegate(self.grid)
        pixmap = QtGui.QPixmap("Mapping_38")
        self.grid.setPixmap(pixmap)
        Imgx=330
        Imgy=65
        self.grid.resize(pixmap.width(),pixmap.height())
        self.grid.move(Imgx,Imgy)
        pass



    def create_tiles(self):
        global CPx, CPy
        self.tiles={}
        letters=["A","B","C","D","E","F"]
        self.btn_grp = QtWidgets.QButtonGroup(self)
        self.btn_grp.setExclusive(True)
        for i in range(0,6):
            self.tiles[i]={}
            for j in range(0,6):
                self.tiles[i][j] = QtWidgets.QPushButton("%s%s"%(letters[i],j+1),self.grid)
                font = QtGui.QFont("Arial", 20, QtGui.QFont.Bold)
                self.tiles[i][j].setFont(font)
                self.tiles[i][j].resize(87,86)
                self.tiles[i][j].move(22+87*(i)+1,39+86*(j)+1)
                self.tiles[i][j].setStyleSheet("QPushButton {background-color: rgba(0, 0, 255, 0);}")
                self.setMouseMoveEventDelegate(self.tiles[i][j])
                self.btn_grp.addButton(self.tiles[i][j])
                pass
            pass
        self.btn_grp.buttonClicked.connect(self.showForm)
        pass

    global ParamLines_V2p2,ParamLines_V3p8
    ParamLines_V2p2={"A1": 13, "A2": 19, "A3": 9, "A4": 14, "A5": 22, "A6": 5, "B1": 2, "B2": 1, "B3": 1, "B4": 2, "B5": 1, "B6": 2,
                        "C1": 1, "C2": 1, "C3": 1, "C4": 1, "C5": 1, "C6": 3, "D1": 1, "D2": 1, "D3": 1, "D4": 1, "D5": 1, "D6": 4,
                        "E1": 2, "E2": 1, "E3": 1, "E4": 2, "E5": 1, "E6": 2, "F1": 8, "F2": 20, "F3": 15, "F4": 11, "F5": 22, "F6": 10}

    ParamLines_V3p8={"A1": 17, "A2": 17, "A3": 11, "A4": 20, "A5": 15, "A6": 12, "B1": 4, "B2": 1,  "B3": 4, "B4": 3, "B5": 1,  "B6": 4,
                        "C1": 1,  "C2": 1,  "C3": 1,  "C4": 1,  "C5": 1,  "C6": 4,  "D1": 2,  "D2": 1,  "D3": 1,  "D4": 1,  "D5": 1,  "D6": 6,
                        "E1": 4,  "E2": 1,  "E3": 6, "E4": 5, "E5": 1,  "E6": 5, "F1": 10, "F2": 16, "F3": 16, "F4": 11, "F5": 22, "F6": 11}


    def tileMenu(self):
        global Imgx,Imgy
        global formX,formY,tile

        if self.flexVersion=="0.0": #When program is launched
            self.CI = QtWidgets.QLabel(self)
            self.setMouseMoveEventDelegate(self.CI)
            self.CI.resize(600,600)
        
            self.tileName = QtWidgets.QLabel(self.CI)
            tileNameFont = QtGui.QFont("Arial", 25)#, QtGui.QFont.Bold) 
            self.tileName.setFont(tileNameFont)
            self.setMouseMoveEventDelegate(self.CI)
            self.tileName.resize(220,220)
            self.tileName.move(350,0)
        
            self.bBack = QtWidgets.QPushButton("Back",self.CI)
            self.bBack.resize(100,50)
            self.bBack.move(250,160)
            self.bBack.clicked.connect(self.hideForm)

            self.bNextTile = QtWidgets.QPushButton("Next Tile",self.CI)
            self.bNextTile.resize(100,50)
            self.bNextTile.move(350,160)
            self.bNextTile.clicked.connect(self.nextTile)
        
            self.ZoomTile = QtWidgets.QPushButton("Zoom Image",self.CI)
            self.ZoomTile.resize(100,50)
            self.ZoomTile.move(450,160)
            self.ZoomTile.clicked.connect(self.open_window)

            self.form = QtWidgets.QLabel(self.CI)
            self.setMouseMoveEventDelegate(self.form)

            self.pixmap = QtGui.QPixmap()


            self.lDisc = QtWidgets.QLabel("Discoloration",self.CI)
            self.lDisc.resize(220,220)
            self.lDisc.move(215,225)
            self.lScra = QtWidgets.QLabel("Scratch",self.CI)
            self.lScra.resize(220,220)
            self.lScra.move(213+90,225)
            self.lCont = QtWidgets.QLabel("Contamination",self.CI)
            self.lCont.resize(220,220)
            self.lCont.move(213+148,225)
            self.lOtherObs = QtWidgets.QLabel("Other Observations",self.CI)
            self.lOtherObs.resize(130,15)
            self.lOtherObs.move(213+148+102,328)

            letters=["A","B","C","D","E","F"]
            self.tileRef={}
            self.tbDisc={}
            self.tbScra={}
            self.tbCont={}
            self.tOtherObs={}
            for i in range(0,6):
                self.tileRef[i]={}
                self.tbDisc[i]={}
                self.tbScra[i]={}
                self.tbCont[i]={}
                self.tOtherObs[i]={}
                for j in range(0,6):                
                    self.tileRef[i][j] = QtWidgets.QLabel(self.CI)
                    self.setMouseMoveEventDelegate(self.tileRef[i][j])
                    pixmap = QtGui.QPixmap("TileRefImages/3.8/FlexV3p8Reference_"+str(letters[i])+str(j+1)+".png")
                    formX=pixmap.width()
                    formY=pixmap.height()

                    w = self.tileRef[i][j].width()
                    h = self.tileRef[i][j].height()
                    self.tileRef[i][j].setPixmap(pixmap)
                    self.tileRef[i][j].resize(600,400)
                    self.tileRef[i][j].move(1200,310)

                    self.tbDisc[i][j]={}
                    self.tbScra[i][j]={}
                    self.tbCont[i][j]={}
                    self.tOtherObs[i][j]=QtWidgets.QTextEdit(self.tileRef[i][j])
                    f = self.tOtherObs[i][j].font()
                    f.setPointSize(10)
                    self.tOtherObs[i][j].setFont(f)
                    self.tOtherObs[i][j].resize(135,170)
                    self.tOtherObs[i][j].move(455,35)
                    for k in range(ParamLines_V3p8[letters[i]+str(j+1)]):
                        self.tbDisc[i][j][k] = QtWidgets.QCheckBox(self.tileRef[i][j])
                        self.tbDisc[i][j][k].setStyleSheet("QCheckBox::indicator { width: 10px; height: 10px;}")
                        self.tbDisc[i][j][k].move(249,35+k*13)

                        self.tbScra[i][j][k] = QtWidgets.QCheckBox(self.tileRef[i][j])
                        self.tbScra[i][j][k].setStyleSheet("QCheckBox::indicator { width: 10px; height: 10px;}")
                        self.tbScra[i][j][k].move(249+74,35+k*13)

                        self.tbCont[i][j][k] = QtWidgets.QCheckBox(self.tileRef[i][j])
                        self.tbCont[i][j][k].setStyleSheet("QCheckBox::indicator { width: 10px; height: 10px;}")
                        self.tbCont[i][j][k].move(249+148,35+k*13)
                        pass
                    pass
                pass
        
            self.CI.move(Imgx+1200,Imgy)
            pass
    
        if self.flexVersion=="2.2": 

            letters=["A","B","C","D","E","F"]
            for i in range(0,6):
                for j in range(0,6):                
                    self.setMouseMoveEventDelegate(self.tileRef[i][j])
                    pixmap = QtGui.QPixmap("TileRefImages/2.2/"+str(letters[i])+str(j+1)+"ref.png")
                    formX=pixmap.width()
                    formY=pixmap.height()

                    w = self.tileRef[i][j].width()
                    h = self.tileRef[i][j].height()
                
                    self.tileRef[i][j].setPixmap(pixmap)

                    if len(range(ParamLines_V3p8[letters[i]+str(j+1)])) > len(range(ParamLines_V2p2[letters[i]+str(j+1)])):

                        for k in range(ParamLines_V3p8[letters[i]+str(j+1)]):
                            if k >= len(range(ParamLines_V2p2[letters[i]+str(j+1)])):
                                self.tbDisc[i][j][k].hide()
                                self.tbScra[i][j][k].hide()
                                self.tbCont[i][j][k].hide()
                                #print("hiding",letters[i],j,k,self.tbDisc[i][j][k].pos())
                            pass

                        pass
                    
                    for k in range(ParamLines_V2p2[letters[i]+str(j+1)]):
                        self.tbDisc[i][j][k] = QtWidgets.QCheckBox(self.tileRef[i][j])
                        self.tbDisc[i][j][k].setStyleSheet("QCheckBox::indicator { width: 10px; height: 10px;}")
                        self.tbDisc[i][j][k].move(249,35+k*13)
                        self.tbDisc[i][j][k].show()

                        self.tbScra[i][j][k] = QtWidgets.QCheckBox(self.tileRef[i][j])
                        self.tbScra[i][j][k].setStyleSheet("QCheckBox::indicator { width: 10px; height: 10px;}")
                        self.tbScra[i][j][k].move(249+74,35+k*13)
                        self.tbScra[i][j][k].show()

                        self.tbCont[i][j][k] = QtWidgets.QCheckBox(self.tileRef[i][j])
                        self.tbCont[i][j][k].setStyleSheet("QCheckBox::indicator { width: 10px; height: 10px;}")
                        self.tbCont[i][j][k].move(249+148,35+k*13)
                        self.tbCont[i][j][k].show()
                        #print("adding",letters[i],j,k,self.tbDisc[i][j][k].pos())
                        pass
                pass
            pass


        if self.flexVersion=="3.8": 
            
            letters=["A","B","C","D","E","F"]
            for i in range(0,6):
                for j in range(0,6):                
                    #self.tileRef[i][j] = QtWidgets.QLabel(self.CI)
                    self.setMouseMoveEventDelegate(self.tileRef[i][j])
                    pixmap = QtGui.QPixmap("TileRefImages/3.8/FlexV3p8Reference_"+str(letters[i])+str(j+1)+".png")
                    formX=pixmap.width()
                    formY=pixmap.height()

                    w = self.tileRef[i][j].width()
                    h = self.tileRef[i][j].height()
                
                    self.tileRef[i][j].setPixmap(pixmap)

                    if len(range(ParamLines_V2p2[letters[i]+str(j+1)])) > len(range(ParamLines_V3p8[letters[i]+str(j+1)])):
                        for k in range(ParamLines_V2p2[letters[i]+str(j+1)]):
                            if k >= len(range(ParamLines_V3p8[letters[i]+str(j+1)])):
                                self.tbDisc[i][j][k].hide()
                                self.tbScra[i][j][k].hide()
                                self.tbCont[i][j][k].hide()
                                #print("hiding",letters[i],j,k,self.tbDisc[i][j][k].pos())
                            pass

                        pass

                    for k in range(ParamLines_V3p8[letters[i]+str(j+1)]):
                            self.tbDisc[i][j][k] = QtWidgets.QCheckBox(self.tileRef[i][j])
                            self.tbDisc[i][j][k].setStyleSheet("QCheckBox::indicator { width: 10px; height: 10px;}")
                            self.tbDisc[i][j][k].move(249,35+k*13)
                            self.tbDisc[i][j][k].show()

                            self.tbScra[i][j][k] = QtWidgets.QCheckBox(self.tileRef[i][j])
                            self.tbScra[i][j][k].setStyleSheet("QCheckBox::indicator { width: 10px; height: 10px;}")
                            self.tbScra[i][j][k].move(249+74,35+k*13)
                            self.tbScra[i][j][k].show()

                            self.tbCont[i][j][k] = QtWidgets.QCheckBox(self.tileRef[i][j])
                            self.tbCont[i][j][k].setStyleSheet("QCheckBox::indicator { width: 10px; height: 10px;}")
                            self.tbCont[i][j][k].move(249+148,35+k*13)
                            self.tbCont[i][j][k].show()
                            #print("adding",letters[i],j,k,self.tbDisc[i][j][k].pos())
                            pass
                        
                    pass
                pass
            pass


    def showForm(self, btn):
        global Imgx, Imgy
        self.grid.move(1200,65)
        #print(btn)
        letters=["A","B","C","D","E","F"]
        for i in range(0,6):
            for j in range(0,6):
                if btn == self.tiles[i][j]:
                    self.tileRef[i][j].move(0,310)
                    self.tileName.setText("Tile "+str(letters[i])+str(j+1))
                    #print(self.tileName.text())
                    self.pixmap = QtGui.QPixmap.fromImage(ImageQt(self.dir_TileImages+"/RD53A_Quad_HPK_11_Assembly_" + str(letters[i])+str(j+1) + ".tiff"))
                    self.pixmap = self.pixmap.scaled(220, 220, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)
                    pass
                pass
            pass
        formX=self.pixmap.width()
        formY=self.pixmap.height()
        self.form.setPixmap(self.pixmap)
        self.form.resize(220,220)
        self.form.move(0,80)
        #pixmap.scaled(20, 10, QtCore.Qt.KeepAspectRatio)
        self.CI.move(Imgx,0)

        pass
        
    def hideForm(self):
        global Imgx, Imgy
        self.grid.move(Imgx,Imgy)
        for i in range(0,6):
            for j in range(0,6): self.tileRef[i][j].move(900,310)
            pass
        self.CI.move(1200,65)
        pass
    
    def nextTile(self):
        letters=["A","B","C","D","E","F"]
        a=999
        b=999
        for i in range(0,6):
            for j in range(0,6):
                if self.tileRef[i][j].pos().x()==0:
                    a=i
                    b=j
                    pass
                pass
            pass
        pass
        if b==5:
            self.tileRef[a][b].move(900,310)
            if a==5:
                a=0
                b=0
                self.tileRef[a][b].move(0,310)
                self.tileName.setText("Tile "+str(letters[a]+str(b+1)))
                self.pixmap = QtGui.QPixmap(self.dir_TileImages+"/RD53A_Quad_HPK_11_Assembly_" + str(letters[a]+str(b+1)) + ".tiff")
                self.pixmap = self.pixmap.scaled(220, 220, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)

            else:
                b=0
                self.tileRef[a+1][b].move(0,310)
                self.tileName.setText("Tile "+str(letters[a+1]+str(b+1)))
                self.pixmap = QtGui.QPixmap(self.dir_TileImages+"/RD53A_Quad_HPK_11_Assembly_" + str(letters[a+1]+str(b+1)) + ".tiff")
                self.pixmap = self.pixmap.scaled(220, 220, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)

            pass
        else:
            self.tileRef[a][b].move(900,310)
            self.tileRef[a][b+1].move(0,310)
            self.tileName.setText("Tile "+str(letters[a]+str(b+2)))
            self.pixmap = QtGui.QPixmap(self.dir_TileImages+"/RD53A_Quad_HPK_11_Assembly_" + str(letters[a]+str(b+2)) + ".tiff")
            self.pixmap = self.pixmap.scaled(220, 220, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)

            pass
        formX=self.pixmap.width()
        formY=self.pixmap.height()
        self.form.setPixmap(self.pixmap)
        self.form.resize(220,220)
        self.form.move(0,80)
        pass
    
    def setMouseMoveEventDelegate(self, setQWidget):
        global CPx, CPy
        def subWidgetMouseMoveEvent (eventQMouseEvent):
            currentQPoint = self.mapFromGlobal(QtGui.QCursor.pos())
            CPx=currentQPoint.x()
            CPy=currentQPoint.y()
            #print(CPx,CPy)
            QtWidgets.QWidget.mouseMoveEvent(setQWidget, eventQMouseEvent)
            for i in range(0,6):
                for j in range(0,6):
                    if CPx>Imgx+23+(87*i) and CPx<Imgx+110+(87*i) and CPy>Imgy+40+(86*j) and CPy<Imgy+126+(86*j):
                        self.tiles[i][j].setStyleSheet("QPushButton {background-color: rgba(0, 0, 255, 100);}")
                        pass
                    else: self.tiles[i][j].setStyleSheet("QPushButton {background-color: rgba(0, 0, 255, 0);}")
                    pass
                pass
            pass
        setQWidget.setMouseTracking(True)
        setQWidget.mouseMoveEvent = subWidgetMouseMoveEvent
        
        pass
    
    def GenRep(self):
        global ParamLines
        letters=["A","B","C","D","E","F"]
        
        filename="Visual_Inspection_Report_Flex_"+self.tID.text()+".tex"
        file = open(filename, "w") 

        if self.rbFlexV2p2.isChecked() == True: 
            self.flexVersion="2.2"
            ParamLines = ParamLines_V2p2
            pixmap_file = 'Mapping.png'
        elif self.rbFlexV3p8.isChecked() == True: 
            self.flexVersion="3.8"
            ParamLines = ParamLines_V3p8
            pixmap_file = 'Mapping_38.png'


        
        tileInspForm=['']
        for i in range(0,6):
            for j in range(0,6):

                if self.flexVersion=="2.2":
                    tileRefImg="TileRefImages/2.2/"+str(letters[i])+str(j+1)+"ref.png"
                if self.flexVersion=="3.8":
                    tileRefImg="TileRefImages/3.8/FlexV3p8Reference_"+str(letters[i])+str(j+1)+".png"
                content=False
                
                for k in range(ParamLines[letters[i]+str(j+1)]):
                    if (self.tbDisc[i][j][k].isChecked()==True) or (self.tbScra[i][j][k].isChecked()==True) or (self.tbCont[i][j][k].isChecked()==True) or (len(self.tOtherObs[i][j].toPlainText()))>0: content=True
                    pass
            
                if content==True:
                    tileInspForm+=['\\begin{overpic}[trim=0 150 0 0,width=1\\textwidth]{'+tileRefImg+'}\n',
                    '%Tile Name\n',
                    '\\put (-4,21) {\\LARGE{'+str(letters[i])+str(j+1)+'}}\n']

                    tileInspForm.append('%Scratch\n')
                    for k in range(ParamLines[letters[i]+str(j+1)]):
                        tileInspForm.append('\\put ('+str(42.7)+','+str(34.06-2.23*k)+') {$\\square$}\n')
                        if self.tbDisc[i][j][k].isChecked()==True:  tileInspForm.append('\\put ('+str(42.9)+','+str(34.06-2.23*k)+') {\\xmark}\n')
                    tileInspForm.append('%Discoloration\n')
                    for k in range(ParamLines[letters[i]+str(j+1)]):
                        tileInspForm.append('\\put ('+str(55.5)+','+str(34.06-2.23*k)+') {$\\square$}\n')
                        if self.tbScra[i][j][k].isChecked()==True:  tileInspForm.append('\\put ('+str(55.7)+','+str(34.06-2.23*k)+') {\\xmark}\n')
                    tileInspForm.append('%Contamination\n')
                    for k in range(ParamLines[letters[i]+str(j+1)]):    
                        tileInspForm.append('\\put ('+str(68)+','+str(34.06-2.23*k)+') {$\\square$}\n')
                        if self.tbCont[i][j][k].isChecked()==True:  tileInspForm.append('\\put ('+str(68.2)+','+str(34.06-2.23*k)+') {\\xmark}\n')
                    tileInspForm.append('%Other Observations\n')
                    for k in range(ParamLines[letters[i]+str(j+1)]):    
                        pass
                    tileInspForm.append('\\put (78,21) {\\parbox{\\dimexpr\\linewidth-125\\fboxsep-3\\fboxrule}{'+str(self.tOtherObs[i][j].toPlainText())+'}}\n')
                        
                    tileInspForm.append('\\end{overpic}\n\n')
                    pass
                pass
            pass
    
        flexID=str(self.tID.text())+"\\ "*(15-len(self.tID.text()))
        reportData=['\\documentclass[11pt,a4paper,notitlepage]{article}\n',
        '\\usepackage{authblk}\n \\usepackage{graphicx}\n',
        '\\usepackage{caption}\n \\usepackage{enumerate}\n',
        '\\usepackage[top=2cm, bottom=2cm, left=2cm, right=2cm]{geometry}\n',
        '\\usepackage{float}\n \\usepackage{enumitem,amssymb}\n',
        '\\newlist{todolist}{itemize}{2}\n',
        '\\setlist[todolist]{label=$\square$}\n',
        '\\usepackage{pifont}\n',
        '\\usepackage[percent]{overpic}\n\n',
        '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n',
        '% Definitions\n',
        '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n',
        '\\newcommand{\\cmark}{\\ding{51}}%\n',
        '\\newcommand{\\xmark}{\\ding{55}}%\n',
        '\\newcommand{\\done}{\\rlap{$\\square$}{\\raisebox{2pt}{\\large\\hspace{1pt}\\cmark}}%\n',
        '\\hspace{-2.5pt}}\n \\newcommand{\\wontfix}{\\rlap{$\\square$}{\\large\\hspace{1pt}\\xmark}}\n\n',
        '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n',
        '% Title\n',
        '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n',
        '\\title{RD53b Flex Visual Inspection Report}\n',
        '\\author[a]{Abhishek Sharma}\n',
        '\\author[a]{Petra Riedler}\n',
        '\\affil[a]{CERN, Switzerland}\n',
        '\\date{\\today}\n\n',
        '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n',
        '%% Body\n',
        '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n',
        '\n\n',
        '\\begin{document} \n',
        '\n',
        '\\maketitle\n',
        '\\thispagestyle{empty}\n',
        '% Overview:\n',
        '% \\\\\n',
        '\\begin{itemize}\n',
        '\\item Flex Version: '+self.flexVersion+'\n',
        '\\item Flex ID: '+self.tID.text()+' \n',
        '\\item Origin: '+str(self.cbOrigin.currentText())+'\n',
        '\\item Produced By: '+self.tProducedBy.text()+'\n',
        '\\item Storage Site: '+str(self.cbStorageSite.currentText())+'\n',
        '\\item Delivery Date: '+self.tDeliveryDate.text()+'\n',
        '\\item Assembly Site: '+str(self.cbAssemblySite.currentText())+'\n',
        '\\item Cleaned By: '+self.tCleanedBy.text()+'\n',
        '\\item Inspection Site: '+str(self.cbInspectionSite.currentText())+'\n',
        '\\item Inspection Date: '+self.tInspectionDate.text()+'\n',
        '\\item Inspection Conducted By: '+self.tInspector.text()+'\n\n',
        '\end{itemize}\n',
        'General Comments: '+self.tGeneralComments.toPlainText()+'\n',
        '\\\\ \\ \\\\ \n',
        'Observations: '+self.tObservations.toPlainText()+'\n',
        '\\begin{figure}[H]\n',
        '\\includegraphics[height=8cm]{%s}\n'%pixmap_file,
        '\\centering\n',
        '\\caption{Flex Inspection Tile Mapping.}\n\n',
        '\\end{figure}\n',
        '\\begin{table}[!ht]\n',
        '\\begin{tabular}{l|c|c|c|c}\n',
        '\\ \\ \\ \\ \\ \\ \\  Flex ID: '+flexID+' \\ \\ \\ \\ \\  & \\ \\ \\ Discoloration \\ \\ & Scratch & Contamination & Other Observations \\\\ \n',
        '\\hline\n',
        '\\end{tabular}\n',
        '\\end{table}\n']

        ImagesSummary=['%Image File Curation\n',
        '\\begin{table}[!ht]\n',
        '\\centering\n',
        '\\begin{tabular}{c|c|c}\n',
        '\n',
        '%\hline\n',
        'Image Filename & File Size & Date Taken \\\\ \\hline\n']
        
        for f in os.listdir("FlexImages/"):
            if flexID in f and ".png" in f:
                f=f.strip()
                imgName=f
                imgDate=datetime.fromtimestamp(os.path.getctime(str("FlexImages/"+imgName))).strftime('%d/%m/%Y')
                if "_" in f: f=f.replace("_","\_")
                ImagesSummary.append(str(f)+' & '+str("%.2f"%(os.path.getsize(str("FlexImages/"+imgName))/1000000.))+'~Mb & '+imgDate+' \\\\ \\hline\n')
                pass
        ImagesSummary.append('\\end{tabular}\n'+'\\caption{Image filenames and metadata for \\textbf{Flex ID: '+str(self.tID.text())+'}.}\n'+'\\end{table}\n')

        reportEnd=['\end{document}']
        
        reportData=reportData+tileInspForm+ImagesSummary+reportEnd

        report=""
        for line in reportData: report+=line
        print(report)
        file.write(report) 
        file.close()
        os.system("pdflatex "+filename)
        dir = "../Reports"
        if not os.path.exists(dir): os.makedirs(dir)
        os.system("mv "+filename[:-4]+".pdf"+" ../Reports/"+filename[:-4]+".pdf")
        pass

    def UploadToPDB(self):
        os.system("python uploader_vi.py")
        #upload_results(code1,code2,mod_name,stage,run_num,date,result1,result2)
        pass
    
    def zoomImg(self):
        global formX, formY
        mydialog = QtWidgets.QDialog(self)
        mydialog.resize(formX,formY)
        self.dir_TileImages = QtWidgets.QFileDialog.getExistingDirectory(None, 'Select project folder:', 'F:\\', QtWidgets.QFileDialog.ShowDirsOnly)
        #mydialog.show()
    
    def saveFields(self):
        
        if self.rbFlexV2p2.isChecked() == True: self.flexVersion="2.2"
        elif self.rbFlexV3p8.isChecked() == True: self.flexVersion="3.8"
        fw=open("VisualInspectionGUIFields_"+self.tID.text()+".txt","w")    
        fw.write("Flex Version: %s\n"%self.flexVersion)
        fw.write("Flex ID: %s\n"%self.tID.text())
        fw.write("Origin: %s\n"%self.cbOrigin.currentText())
        fw.write("Produced By: %s\n"%self.tProducedBy.text())
        fw.write("Storage Site: %s\n"%self.cbStorageSite.currentText())
        fw.write("Delivery Date: %s\n"%self.tDeliveryDate.text())
        fw.write("Assembly Site: %s\n"%self.cbAssemblySite.currentText())
        fw.write("Cleaned By: %s\n"%self.tCleanedBy.text())
        fw.write("Inspection Site: %s\n"%self.cbInspectionSite.currentText())
        fw.write("Inspection Date: %s\n"%self.tInspectionDate.text())
        fw.write("Inspection Conducted By: %s\n"%self.tInspector.text())
        fw.write("General Comments: %s\n"%self.tGeneralComments.toPlainText())
        fw.write("Observations: %s\n"%self.tObservations.toPlainText())
        fw.close()
        pass
        
    def loadFields(self):
        fr = open("VisualInspectionGUIFields_"+self.tID.text()+".txt","r")
        for line in fr.readlines():
            line=line.strip()
            print(line)
            if "Flex Version: 2.2" in line: self.rbFlexV2p2.setChecked(True)
            elif "Flex Version: 3.8" in line: self.rbFlexV3p8.setChecked(True)
            if "Flex ID: " in line: self.tID.setText(line.split(": ")[1])
            if "Origin: " in line:
                index = self.cbOrigin.findText(line.split(": ")[1], QtCore.Qt.MatchFixedString)
                if index >= 0: self.cbOrigin.setCurrentIndex(index)
                pass
            if "Produced By: " in line: self.tProducedBy.setText(line.split(": ")[1])
            if "Storage Site: " in line:
                print("*********")
                print("CERN SMD Lab",len("CERN SMD Lab"))
                print(line.split(": ")[1],len(line.split(": ")[1]))
                print("*********")
                index = self.cbStorageSite.findText(line.split(": ")[1], QtCore.Qt.MatchFixedString)
                if index >= 0: self.cbStorageSite.setCurrentIndex(index)
                pass
            if "Delivery Date: " in line:
                date=QtCore.QDate.fromString(line.split(": ")[1].replace(".",""),"ddMMyy")
                self.tDeliveryDate.setDate(date)
                pass
            if "Assembly Site: " in line:
                index = self.cbAssemblySite.findText(line.split(": ")[1], QtCore.Qt.MatchFixedString)
                if index >= 0: self.cbAssemblySite.setCurrentIndex(index)
                pass
            if "Cleaned By: " in line: self.tCleanedBy.setText(line.split(": ")[1])
            if "Inspection Site: " in line:
                index = self.cbInspectionSite.findText(line.split(": ")[1], QtCore.Qt.MatchFixedString)
                if index >= 0: self.cbInspectionSite.setCurrentIndex(index)
                pass
            if "Inspection Date: " in line:
                date=QtCore.QDate.fromString(line.split(": ")[1].replace(".",""),"ddMMyy")
                self.tInspectionDate.setDate(date)
                pass
            if "Inspection Conducted By: " in line: self.tInspector.setText(line.split(": ")[1])
            if "General Comments: " in line: self.tGeneralComments.setText(line.split(": ")[1])
            if "Observations: " in line: self.tObservations.setText(line.split(": ")[1])
            pass
        fr.close()
    

    def open_window(self, checked):
        global formX, formY
        mydialog = QtWidgets.QDialog(self)

        #mydialog.resize(formX,formY)
        print("Zoom Image: ",self.tileName.text().replace("Tile ",""))
        
        layout = QtWidgets.QVBoxLayout()

        pixmap_ZoomIn = QtGui.QPixmap.fromImage(ImageQt(self.dir_TileImages+"/RD53A_Quad_HPK_11_Assembly_" + self.tileName.text().replace("Tile ","") + ".tiff"))
        imageLabel = QtWidgets.QLabel()

        formX=pixmap_ZoomIn.width()
        formY=pixmap_ZoomIn.height()
        pixmap_ZoomIn_scaled = pixmap_ZoomIn.scaled(1500, 600, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)
        imageLabel.setPixmap(pixmap_ZoomIn_scaled)
        imageLabel.resize(1800,700)
        imageLabel.move(300,300)

        layout.addWidget(imageLabel)
        mydialog.setLayout(layout)

        mydialog.show()



    '''
    def enable(self,psu):
        print("PSU %s output %r ")%(psu,self.tOutp[psu].isChecked())
        self.controller.setOutput(psu,self.tOutp[psu].isChecked())
        pass

    def read(self,psu):
        self.statusBar().showMessage("Reading values...")
        QtWidgets.QApplication.processEvents()
        iena = self.controller.isEnabled(psu)
        self.tOutp[psu].blockSignals(True)
        self.tOutp[psu].setChecked(iena)
        self.tOutp[psu].blockSignals(False)
        vset = self.controller.getSetVoltage(psu)
        self.tVset[psu].setText(str(vset))
        clim = self.controller.getCurrentLimit(psu)            
        self.tIlim[psu].setText(str(clim))
        vout = self.controller.getVoltage(psu)
        self.tVout[psu].setText(str(vout))
        cout = self.controller.getCurrent(psu)
        self.tIout[psu].setText(str(cout))
        self.statusBar().showMessage("Last updated: "+time.strftime("%Y-%m-%d %H:%M:%S"))
        QtWidgets.QApplication.processEvents()
        pass

    
    def write(self,psu):
        self.statusBar().showMessage("Writing values...")
        QtWidgets.QApplication.processEvents()
        try:
            val = "%.3f"%float(self.tVset[psu].text())
            self.tVset[psu].setStyleSheet("")
            self.controller.rampVoltage(psu,float(val))
        except ValueError:
            self.tVset[psu].setStyleSheet("background: red;")
            pass
        try:
            val = "%.3f"%float(self.tIlim[psu].text())
            self.tIlim[psu].setStyleSheet("")
            self.controller.setCurrentLimit(psu,float(val))
        except ValueError:
            self.tIlim[psu].setStyleSheet("background: red;")
            pass
        self.statusBar().showMessage("Last updated: "+time.strftime("%Y-%m-%d %H:%M:%S"))
        QtWidgets.QApplication.processEvents()
        pass

    
    def close_application(self):
        choice = QtGui.QMessageBox.question(self, "Quitting...",
                                            "Are you sure you wish to quit?",
                                            QtGui.QMessageBox.Yes |
                                            QtGui.QMessageBox.No)
        if choice == QtGui.QMessageBox.Yes:
            print("Quitting...")
            sys.exit()
        else: pass
        pass
    '''


if __name__=="__main__":

    if False:
        rerun = False
        req_enum="%(LCG_INST_PATH)s/LCG_94/enum34/1.1.6/%(CMTCONFIG)s/lib/python2.7/site-packages"%os.environ
        req_free="%(LCG_INST_PATH)s/LCG_94/freetype/2.6.3/%(CMTCONFIG)s/lib"%os.environ
        if not req_enum in sys.path:
            print("Enum not found")
            os.environ['PYTHONPATH']+=":"+req_enum
            rerun=True
            pass
        if not req_free in os.environ['LD_LIBRARY_PATH']:
            print("Free not found")
            os.environ['LD_LIBRARY_PATH']=req_free+":"+os.environ['LD_LIBRARY_PATH']
            rerun=True
            pass
        if rerun:
            os.execve(os.path.realpath(__file__), sys.argv, os.environ)
            pass
        pass
    
    #signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QtWidgets.QApplication(sys.argv)
    gui = PixFlexVisInsp()
    #def aboutToQuit():
    #    return gui.close_application()
    sys.exit(app.exec_())


